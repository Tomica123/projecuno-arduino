
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif
#include <SoftwareSerial.h>
#include <ESP8266wifi.h>
#include <ArduinoJson.h>
// ESP
#define sw_serial_rx_pin 2 //  Connect this pin to TX on the esp8266
#define sw_serial_tx_pin 3 //  Connect this pin to RX on the esp8266
#define esp8266_reset_pin 5 // Connect this pin to CH_PD on the esp8266, not reset. (let reset be unconnected)
//NEOPIXEL
#define NUMPIXELS      12
#define PIN            6 //Which pin on the Arduino is connected to the NeoPixels?
//ARDUINO2
#define interruptPin 4
#define signalizationPin 5
//MSG FROM/TO SERVER
#define MSG_LENGHT 50
//TODO izmjeriti i postaviti analogna ucitanja
#define IRSENSOR_DISTANCE_FILTER 10
#define IRsensor A0

const char SSID PROGMEM = "blaz";
const char PASSWORD PROGMEM = "tomek123";
const char SERVER_IP PROGMEM = "127.0.0.1";
const char SERVER_PORT PROGMEM = "2001\"";

SoftwareSerial swSerial(sw_serial_rx_pin, sw_serial_tx_pin);
ESP8266wifi wifi(swSerial, swSerial, esp8266_reset_pin, Serial);//adding Serial enabled local echo and wifi debug

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

StaticJsonBuffer<200> jsonBuffer;

void menageSecondArduino(byte contorlStep,byte signalizationMode=0);//control second arduino 0 use for start flashing,1 is use for interrapt

void setup() {

	pinMode(IRsensor,INPUT);
	swSerial.begin(19200);
	Serial.begin(19200);
	while (!Serial);
	wifi.endSendWithNewline(true);
	wifi.begin();
	wifi.connectToAP(SSID, PASSWORD);
	wifi.setTransportToTCP();
	wifi.connectToServer(SERVER_IP,SERVER_PORT);
	wifi.endSendWithNewline(true);

	pixels.begin();// This initializes the NeoPixel library.
}

void loop() {

	WifiMessage in = wifi.listenForIncomingMessage(6000);

	while (!in.hasData) {
		 //TODO ERROR nije dobio poruku 
		 in = wifi.listenForIncomingMessage(6000);
	} 

    if (in.channel != SERVER);	//TODO ERROR poruka nije sa servera

    JsonObject& root = jsonBuffer.parseObject(in.message);
    if (!root.success()); //TODO error poruka nije parsano
   
	long interval    	      = root["interval"];
	byte signalizationMode    = root["signalizationMode"];
	byte distance             = root["distance"];

    startWithTrening(interval,signalizationMode,distance);

}


void startWithTrening(long interval,byte signalizationMode,short distance){
	int startTime=millis();
	int endTime=0;	
	menageSecondArduino(0,signalizationMode);
	if(checkPresence(interval,distance)){
		menageSecondArduino(1);
		endTime=millis()-startTime;
		sendMsgToServer(endTime);
	}else
		sendMsgToServer(endTime);
   		    return;
}


bool checkPresence(int timeout,short distance){
	int startTime=millis();
	while(millis()<startTime+timeout){
		int ValueIRSensor=analogRead(IRsensor);
		if(ValueIRSensor>IRSENSOR_DISTANCE_FILTER && ValueIRSensor<distance)
			return true;
	}
	menageSecondArduino(1);
	        return false;
}

void sendMsgToServer(int time){
	char jsonRoot[MSG_LENGHT];
	while(!wifi.send(SERVER,makeJsonMsg(jsonRoot,time)));
		return;
}

char* makeJsonMsg(char* jsonRoot,int time){
	char buff[MSG_LENGHT];
	strcpy(jsonRoot,"\"SensorTime\":");
	sprintf(buff,"%d",time);
	strcat(jsonRoot,buff);
	strcpy(buff,",\"BatteryLevel\":");
	strcat(jsonRoot,buff);
	sprintf(buff,"%d",checkBatteryLevel());
	strcat(jsonRoot,buff);
    	return jsonRoot;
}

void menageSecondArduino(byte contorlStep,byte signalizationMode=0){
  if(!contorlStep){
    digitalWrite(signalizationPin, signalizationMode);
     return;
  }
  digitalWrite(interruptPin, LOW);
    return;
}

int checkBatteryLevel(){
	//TODO
}






